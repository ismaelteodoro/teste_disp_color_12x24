#include <avr/pgmspace.h>
#include "imagesNumericas.h"

//#define CLK  8   // USE THIS ON ARDUINO UNO, ADAFRUIT METRO M0, etc.
//#define CLK A4 // USE THIS ON METRO M4 (not M0)
#define CLK 11 // USE THIS ON ARDUINO MEGA
#define OE   9
#define LAT 10
#define A   A0
#define B   A1
#define C   A2
#define D   A3

uint8_t r=0, g=0, b=0;

//Quanto menos mais brilho
uint8_t dark, darkMASK=230;
uint8_t flag = 0;

uint8_t numRandom, cores, chaveia = 0;
uint8_t numUnidade, numDezena;


void limpa_display()
{
  digitalWrite(24, LOW); // RED
  digitalWrite(25, LOW); // RED
  digitalWrite(26, LOW); // RED

  digitalWrite(A, LOW); // 
  digitalWrite(B, LOW); //
  for(int x=0;x<192;x++){
    digitalWrite(CLK, HIGH); // Low
    digitalWrite(CLK, LOW); // Low
  }

  digitalWrite(A, HIGH); // 
  digitalWrite(B, LOW); //
  for(int x=0;x<192;x++){
    digitalWrite(CLK, HIGH); // Low
    digitalWrite(CLK, LOW); // Low
  }

  digitalWrite(A, LOW); // 
  digitalWrite(B, HIGH); //
  for(int x=0;x<192;x++){
    digitalWrite(CLK, HIGH); // Low
    digitalWrite(CLK, LOW); // Low
  }
  digitalWrite(LAT, HIGH); //    
  digitalWrite(LAT, LOW); //
}

//seta a cor de 0 a 7
void seta_cor(uint8_t cor){

  switch(cor){
    case 0 :
      digitalWrite(24, LOW); // RED
      digitalWrite(25, LOW); // GREEN
      digitalWrite(26, LOW); // BLUE 
    break;

    case 1 :
      digitalWrite(24, HIGH); // RED
      digitalWrite(25, LOW); // GREEN
      digitalWrite(26, LOW); // BLUE 
    break;

    case 2 :
      digitalWrite(24, LOW); // RED
      digitalWrite(25, HIGH); // GREEN
      digitalWrite(26, LOW); // BLUE 
    break;

    case 3 :
      digitalWrite(24, HIGH); // RED
      digitalWrite(25, HIGH); // GREEN
      digitalWrite(26, LOW); // BLUE 
    break;

    case 4 :
      digitalWrite(24, LOW); // RED
      digitalWrite(25, LOW); // GREEN
      digitalWrite(26, HIGH); // BLUE 
    break;

    case 5 :
      digitalWrite(24, HIGH); // RED
      digitalWrite(25, LOW); // GREEN
      digitalWrite(26, HIGH); // BLUE 
    break;

    case 6 :
      digitalWrite(24, LOW); // RED
      digitalWrite(25, HIGH); // GREEN
      digitalWrite(26, HIGH); // BLUE 
    break;

    case 7 :
      digitalWrite(24, HIGH); // RED
      digitalWrite(25, HIGH); // GREEN
      digitalWrite(26, HIGH); // BLUE 
    break;

  }

}

void load_image0(uint8_t unidade, uint8_t dezena, uint8_t collor){
  //matrix 8 x 12 
  //com 3 linhas
  uint8_t byte,h,x;
  digitalWrite(A, LOW); // LINHA ZERO 
  digitalWrite(B, LOW); // LINHA ZERO
  for( h=0; h<12; h++ )
  {
    byte = pgm_read_byte( &(IMAGES_LINE1[unidade][h]) );
    for( x=0; x<8; x++ )
    {
      if( byte & 1 ){
        seta_cor(collor);    
      }else{
        seta_cor(0); 
      }
      byte >>= 1;
      digitalWrite(CLK, HIGH); // Low
      digitalWrite(CLK, LOW); // Low
    }
  }
  for( h=0; h<12; h++ )
  {
    byte = pgm_read_byte( &(IMAGES_LINE1[dezena][h]) );
    for( x=0; x<8; x++ )
    {
      if( byte & 1 ){
        seta_cor(collor);    
      }else{
        seta_cor(0); 
      }
      byte >>= 1;
      digitalWrite(CLK, HIGH); // Low
      digitalWrite(CLK, LOW); // Low
    }
  }
  digitalWrite(LAT, HIGH); //    
  digitalWrite(LAT, LOW); //
}

void load_image1(uint8_t unidade, uint8_t dezena, uint8_t collor){
    //matrix 8 x 12 
  //com 3 linhas
  uint8_t byte,h,x;
  digitalWrite(A, HIGH); // LINHA UM 
  digitalWrite(B, LOW); // LINHA UM
  for( h=0; h<12; h++ )
  {
    byte = pgm_read_byte( &(IMAGES_LINE2[unidade][h]) );
    for( x=0; x<8; x++ )
    {
      if( byte & 1 ){
        seta_cor(collor);    
      }else{
        seta_cor(0);
      }
      byte >>= 1;
      digitalWrite(CLK, HIGH); // Low
      digitalWrite(CLK, LOW); // Low
    }
  }
  for( h=0; h<12; h++ )
  {
    byte = pgm_read_byte( &(IMAGES_LINE2[dezena][h]) );
    for( x=0; x<8; x++ )
    {
      if( byte & 1 ){
        seta_cor(collor);    
      }else{
        seta_cor(0);
      }
      byte >>= 1;
      digitalWrite(CLK, HIGH); // Low
      digitalWrite(CLK, LOW); // Low
    }
  }
  digitalWrite(LAT, HIGH); //    
  digitalWrite(LAT, LOW); //
}

void load_image2(uint8_t unidade, uint8_t dezena, uint8_t collor){
    //matrix 8 x 12 
  //com 3 linhas
  uint8_t byte,h,x;
  digitalWrite(A, LOW); // LINHA DOIS 
  digitalWrite(B, HIGH); // LINHA DOIS
  for( h=0; h<12; h++ )
  {
    byte = pgm_read_byte( &(IMAGES_LINE3[unidade][h]) );
    for( x=0; x<8; x++ )
    {
      if( byte & 1 ){
        seta_cor(collor);     
      }else{
        seta_cor(0); 
      }
      byte >>= 1;
      digitalWrite(CLK, HIGH); // Low
      digitalWrite(CLK, LOW); // Low
    }
  }
  for( h=0; h<12; h++ )
  {
    byte = pgm_read_byte( &(IMAGES_LINE3[dezena][h]) );
    for( x=0; x<8; x++ )
    {
      if( byte & 1 ){
        seta_cor(collor);     
      }else{
        seta_cor(0); 
      }
      byte >>= 1;
      digitalWrite(CLK, HIGH); // Low
      digitalWrite(CLK, LOW); // Low
    }
  }
  digitalWrite(LAT, HIGH); //    
  digitalWrite(LAT, LOW); //
}

void escreve_numero(uint8_t collor){
  switch(chaveia){
    case 0:
      load_image0(numUnidade,numDezena,collor);
      chaveia=1;
    break;
    case 1:
      load_image1(numUnidade,numDezena,collor);
      chaveia=2;
    break;
    case 2:
      load_image2(numUnidade,numDezena,collor);
      chaveia=0;
    break;
  }
  
}

ISR(TIMER1_OVF_vect){
  if(flag){
    if(--dark<darkMASK){
      flag=0;
    }
  }else{
    if(++dark>253){
      flag = 1;  
    }
  }
  analogWrite(OE,dark);
}

ISR(TIMER5_OVF_vect){
  TCNT5 = 0xc800;
  escreve_numero(cores);
}


void setup() {

  // Enable all comm & address pins as outputs, set default states:
  pinMode(CLK, OUTPUT);
  digitalWrite(CLK, LOW); // Low
  pinMode(LAT, OUTPUT);
  digitalWrite(LAT, LOW); // Low
  pinMode(OE, OUTPUT);
  digitalWrite(OE, HIGH); // Low //Disable
  pinMode(A, OUTPUT);
  digitalWrite(A, LOW); // Low
  pinMode(B, OUTPUT);
  digitalWrite(B, LOW); // Low
  pinMode(C, OUTPUT);
  digitalWrite(C, LOW); // Low  
  pinMode(D, OUTPUT);
  digitalWrite(D, LOW); // Low

  pinMode(24, OUTPUT);
  digitalWrite(24, LOW); // RED
  pinMode(25, OUTPUT);
  digitalWrite(25, LOW); // GREEN
  pinMode(26, OUTPUT);
  digitalWrite(26, LOW); // BLUE

  limpa_display();

  chaveia = 0;

  TCCR1A = 0b00000000; // 
  TCCR1B = 0b00000011; //

  TCCR5A = 0b00000000; //
  TCCR5B = 0b00000010; //

  TIMSK1 = 0b00000001; //Interrupção por overflow do timer 1
  TIMSK5 = 0b00000001; //Interrupção por overflow do timer 1
  

  dark = darkMASK;
  numRandom = 0;
  cores = 0;
  analogWrite(OE,dark);//brilho atualizado dentro da interrupcao

  

}

#define VALUE_DEC 65535 * 10
uint32_t dec = VALUE_DEC ;
void loop() {
  if(!--dec){
    dec = VALUE_DEC;
    numRandom=random(99);
    if(++cores>7)cores=1;
    noInterrupts();
    limpa_display();
    interrupts();
    numDezena = numRandom/10;
    numUnidade = numRandom%10;
  }
}

